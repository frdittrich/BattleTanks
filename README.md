# BattleTanks
An unreal engine 4 project

## Summary
A tank game project focusing on implementing tank behavior and basic AI enemies through usage of C++.

## Preview
Click on image below to open the video

[![](http://img.youtube.com/vi/xKZuddxtGVA/0.jpg)](https://youtu.be/xKZuddxtGVA "Battletank Demo Video")
